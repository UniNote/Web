import { Component } from '@angular/core';
import { LoginService } from 'src/app/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'UniNote-Web';

  constructor(private loginService: LoginService) { }

  isUserLoggedIn():boolean {
    return this.loginService.isLoggedIn();
  }
}
