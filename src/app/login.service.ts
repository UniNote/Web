import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token : string;

  constructor(private loginService: LoginService) { }

  isLoggedIn(): boolean
  {
    return false;
  }
}
